package ru.kombitsi.tm.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.kombitsi.tm")
@EnableJpaRepositories("ru.kombitsi.tm.repository")
@PropertySource("classpath:application.properties")
public class DataConfig {

    @Bean
    public DataSource dataSource(
            @Value("${datasource.driver}") final String dataSourceDriver,
            @Value("${datasource.url}") final String dataSourceUrl,
            @Value("${datasource.user}") final String dataSourceUser,
            @Value("${datasource.password}") final String dataSourcePassword
    ) {
        final DriverManagerDataSource dataSource =
                new DriverManagerDataSource();
        dataSource.setDriverClassName(dataSourceDriver);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setUsername(dataSourceUser);
        dataSource.setPassword(dataSourcePassword);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            final DataSource dataSource,
            @Value("${hibernate.show_sql}") final boolean showSql,
            @Value("${hibernate.hbm2ddl.auto}") final String tableStrategy,
            @Value("${hibernate.dialect}") final String dialect,
            @Value("${hibernate.cache.use_second_level_cache}") final boolean secondLvCache,
            @Value("${hibernate.cache.use_query_cache}") final boolean queryCache,
            @Value("${hibernate.cache.use_minimal_puts}") final boolean minimalPuts,
            @Value("${hibernate.cache.hazelcast.use_lite_member}") final boolean liteMember,
            @Value("${hibernate.cache.region_prefix}") final String regionPrefix,
            @Value("${hibernate.cache.region.factory_class}") final String factoryClass,
            @Value("${hibernate.cache.provider_configuration_class}") final String configClass
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.kombitsi.tm.");
        final Properties properties = new Properties();
        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.hbm2ddl.auto", tableStrategy);
        properties.put("hibernate.dialect", dialect);
//        properties.put("hibernate.cache.use_second_level_cache", secondLvCache);
//        properties.put("hibernate.cache.use_query_cache", queryCache);
//        properties.put("hibernate.cache.use_minimal_puts", minimalPuts);
//        properties.put("hibernate.cache.hazelcast.use_lite_member", liteMember);
//        properties.put("hibernate.cache.region_prefix", regionPrefix);
//        properties.put("hibernate.cache.region.factory_class", factoryClass);
//        properties.put("hibernate.cache.provider_configuration_class", configClass);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public JpaTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean lm) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(lm.getObject());

        return transactionManager;
    }
}
