package ru.kombitsi.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.dto.ProjectDto;

import java.util.List;

public interface IProjectService {
    @Nullable
    List<ProjectDto> listAllProject();

    @Nullable
    ProjectDto updateProject(@NotNull ProjectDto project);

    @Nullable
    ProjectDto findOneProjectById(@NotNull String id);

    void removeOneProject(@NotNull ProjectDto projectDto);
}
