package ru.kombitsi.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;
import ru.kombitsi.tm.enumerate.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "app_project")
@NoArgsConstructor
public final class Project extends AbstractEntity {
    private String name;
    private String description;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateAdd = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Column(name = "status")
    private Status displayName = Status.SCHEDULE;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    private List<Task> taskList;
}