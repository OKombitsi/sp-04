package ru.kombitsi.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kombitsi.tm.entity.Project;


public interface ProjectRepository extends JpaRepository<Project, String> {
}