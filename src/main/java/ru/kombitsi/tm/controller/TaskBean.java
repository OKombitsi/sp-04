package ru.kombitsi.tm.controller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kombitsi.tm.api.ITaskService;
import ru.kombitsi.tm.dto.ProjectDto;
import ru.kombitsi.tm.dto.TaskDto;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.util.List;

@Named
@Getter
@Setter
@SessionScoped
public class TaskBean {
    @Autowired
    private ITaskService taskService;

    private TaskDto currentTask;

    public void load(String id) throws Exception {
        if (id == null || id.isEmpty()) {
            currentTask = new TaskDto();
        } else {
            currentTask = taskService.getTaskById(id);
        }
    }

    @Nullable
    public List<TaskDto> showAllTaskByAllUsers() {

        return taskService.showAllTaskByAllUsers();
    }

    @Nullable
    public TaskDto getTaskById(@NotNull String id) throws Exception {
        if (id == null) return null;
        currentTask = taskService.getTaskById(id);
        return currentTask;
    }

    @Nullable
    public String updateTask(@NotNull TaskDto currentTask) {
        if (currentTask == null) {
            currentTask = new TaskDto();
        }
        taskService.updateTask(currentTask);
        return "pretty:taskList";
    }

    public String removeOneTask(@NotNull String id) throws Exception {
        if (id == null) return null;
        TaskDto taskDto = taskService.getTaskById(id);
        taskService.removeOneTask(taskDto);
        return "pretty:taskList";
    }
}
