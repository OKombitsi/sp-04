package ru.kombitsi.tm.controller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kombitsi.tm.api.IProjectService;
import ru.kombitsi.tm.dto.ProjectDto;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.util.List;

@Named
@Getter
@Setter
@SessionScoped
public class ProjectBean {

    @Autowired
    private IProjectService projectService;

    private ProjectDto currentProject;

    public void load(String id) {
        if (id == null || id.isEmpty()) {
            currentProject = new ProjectDto();
        }
        else {
            currentProject = projectService.findOneProjectById(id);
        }
    }

    @Nullable
    public List<ProjectDto> listAllProject() {
        List<ProjectDto> projectDtoList = projectService.listAllProject();
        return projectDtoList;
    }

    @Nullable
    public String updateProject(@Nullable ProjectDto currentProject) {
        if (currentProject == null) return null;
        projectService.updateProject(currentProject);
        return "pretty:projectList";
    }

    @Nullable
    public ProjectDto findOneProjectById(@NotNull String id) {
        if (id.isEmpty() || id == null) return null;
        currentProject = projectService.findOneProjectById(id);
        return currentProject;
    }

    public String removeOneProject(@NotNull String id) {
        if (id == null) return null;
        ProjectDto projectDto = projectService.findOneProjectById(id);
        projectService.removeOneProject(projectDto);
        return "pretty:projectList";
    }

}
